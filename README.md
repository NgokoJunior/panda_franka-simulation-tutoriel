this tutorial is only avaiable for ubuntu user

this is a tutorial for simulation of panda_franka in gazebo and moveit through ros1.


this is a primary documentation for panda_franka project.

i personally work through and docker contains ros1

**Instructions**


Dependencies

this is primary dependencies required to use this project

 Ros1 [noetic](http://wiki.ros.org/noetic/Installation/Ubuntu)
 
 you can also work in a environement already provide with ros1 and ubuntu 20, it is already build in this dockerfile  

 **Building**

 
_from the ros repositories_

Binary package for libafranka and franka_ros are avaiable from the ros repositories
`sudo apt install ros-noetic-libfranka ros-noetic-franka-ros`

_buildlding libafranka_

Before building from source, please uninstall existing installations of libfranka and franka_ros to avoid conflicts
`sudo apt remove "*libfranka*"`

To build libfranka, install the following dependencies from Ubuntu’s package manager:

`sudo apt install build-essential cmake git libpoco-dev libeigen3-dev`

for panda_franka you need to clone

```
git clone --recursive https://github.com/frankaemika/libfranka # only for panda
cd libfranka
```
In the source directory, create a build directory and run CMake:

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTS=OFF ..
cmake --build .
```

Optionally (but recommended), a libfranka Debian package can be built using the following command in the same directory:

`cpack -G DEB`

This creates libfranka-<version>-<architecture>.deb. This package can then be installed with:
`sudo dpkg -i libfranka*.deb`

_Building Ros package_

```
cd /path/to/desired/folder
mkdir -p catkin_ws/src
cd catkin_ws
source /opt/ros/noetic/setup.sh
catkin_init_workspace src
```
Then clone the franka_ros repository from [GitHub](https://github.com/frankaemika/franka_ros):


`git clone --recursive https://github.com/frankaemika/franka_ros src/franka_ros`

Within your catkin workspace, download package:

```
cd catkin_ws/src
git clone https://github.com/ros-planning/moveit_tutorials.git -b master
git clone https://github.com/ros-planning/panda_moveit_config.git -b noetic-devel
```

**Build your Catkin Workspace**
The following will install from Debian any package dependencies not already in your workspace:

```
cd catkin_ws/src
rosdep install -y --from-paths . --ignore-src --rosdistro noetic
```

The next command will configure your catkin workspace:

```
cd catkin_ws
catkin config --extend /opt/ros/${ROS_DISTRO} --cmake-args -DCMAKE_BUILD_TYPE=Release
catkin build
```
Source the catkin workspace:

`source devel/setup.bash`


<details><summary>Modify the world</summary>

`cd catkin_ws/src/panda_moveit_confg/launch `

edit the launch file demo_gazebo.launch, add a gazebo world in the 7th line 

```
<!-- Gazebo specific options -->
  <arg name="gazebo_gui" default="true" />
  <arg name="paused" default="false" />
  <!-- overwriting these args -->
  <arg name="debug" default = "false"/>
  <arg name="gui" default = "false"/>
  <arg name="pause" default = "true"/>
  <arg name="world" default = "$(find panda_moveit_config)/worlds/empty_world.world"/>
```

in the package panda_moveit_config , create a folder world, inside this  folder , create a a world file an edit it 

```
cd catkin_ws/src/panda_moveit_config
mkdir worlds && cd worlds
vim empty_world.world
```
insert follow command :

```
<?xml version= "1.0" ?>
<sdf version="1.5">
   <world name="default">
    <!-- a global light source -->
    <include>
     <uri>model://sun</uri>
    </include>
    <!-- a ground plane -->
     <include>
      <uri>model://ground_plane</uri>
    </include>
    <include>
            <uri>model://wood_cube_10cm</uri>
            <pose>0.6 0.2 0 0 0 0</pose>
            <size>1.5 0.8 0.03</size>
    </include>
```


 </world>
</sdf>

now you can edit your gazebo world and add or remove an object
</details>


<details><summary>Examples</summary>
in  shell A run 

```
cd catkin_ws
roslaunch panda_moveit_config demo_gazebo.launch 
```


in shell B run 
` rosrun moveit_tutorials move_group_interface_tutorial`


to have a good knowledge of moveit, read this tutorial [moveit1](https://ros-planning.github.io/moveit_tutorials/doc/move_group_interface/move_group_interface_tutorial.html)
</details>

